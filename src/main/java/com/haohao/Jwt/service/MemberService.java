package com.haohao.Jwt.service;

import java.util.List;

import com.haohao.Jwt.entity.Member;
import com.haohao.Jwt.model.MemberModel;

public interface MemberService {
    List<Member> getAllMembers();
    
    MemberModel getMemberByUserName(String userName);
    
    boolean addMember(MemberModel memberModel);
}
