package com.haohao.Jwt.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.haohao.Jwt.dao.MemberDao;
import com.haohao.Jwt.entity.Member;

@Repository
@Transactional
public class memberDaoImpl implements MemberDao {

    // connect to database
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Member> queryAllMembers() {
        TypedQuery<Member> namedQuery = entityManager.createNamedQuery("find_all_member", Member.class);
        return namedQuery.getResultList();
    }

    @Override
    public Member queryMamberByUserName(String userName) {
        List<Member> list = new ArrayList<Member>();
        try {
            CriteriaBuilder qb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Member> cq = qb.createQuery(Member.class);
            Root<Member> root = cq.from(Member.class);
            cq.where(qb.equal(root.get("userName"), userName));
            list = entityManager.createQuery(cq).getResultList();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public boolean insertMember(Member member) {
        try {
            entityManager.persist(member);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
