package com.haohao.Jwt.config;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.haohao.Jwt.model.MemberModel;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Configuration
public class JwtTokenUtil implements Serializable {
//  JWT = header.payload.signature
//    header = {
//         "alg": "HS256",
//         "typ": "JWT"
//     }
//    payload = {
//        "sub":"wang",
//        "created":1489079981393,
//        "exp":1489684781
//    }
//    這裡放聲明內容，可以說就是存放溝通訊息的地方，在定義上有 3 種聲明 (Claims)
//
//    Reserved (註冊聲明)
//    Public (公開聲明)
//    Private (私有聲明)
//    註冊聲明參數 (建議但不強制使用)
//
//    Claim
//    iss (Issuer) - jwt簽發者
//    sub (Subject) - jwt所面向的用戶
//    aud (Audience) - 接收jwt的一方
//    exp (Expiration Time) - jwt的過期時間，這個過期時間必須要大於簽發時間
//    nbf (Not Before) - 定義在什麼時間之前，該jwt都是不可用的
//    iat (Issued At) - jwt的簽發時間
//    jti (JWT ID) - jwt的唯一身份標識，主要用來作為一次性token,從而迴避重放攻擊

//    signature = {
//        HMACSHA256(
//        base64UrlEncode(header) + "." +
//        base64UrlEncode(payload),
//        secret)
//    }
    private static final long serialVersionUID = -2550185165626007488L;
    public static final long JWT_TOKEN_VALIDITY = 60 * 60;
    @Value("${jwt.secret}")
    private String secret;
    
    // generate token for user
    public String generateToken(MemberModel memberModel) {
        System.out.println(memberModel.toString());
        Map<String, Object> claims = new HashMap<>();
        claims.put("memberId", memberModel.getId());
        claims.put("name", memberModel.getName());
        claims.put("email", memberModel.getEmail());
        return doGenerateToken(claims, memberModel.getUserName());
    }

//while creating the token -
//1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
//2. Sign the JWT using the HS256 algorithm and secret key.
//3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
//   compaction of the JWT to a URL-safe string 
    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    // validate token
    public Claims validateToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }
}
