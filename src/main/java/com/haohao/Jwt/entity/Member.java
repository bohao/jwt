package com.haohao.Jwt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@NamedQuery(name="find_all_member", query="select m from Member m")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@Data
public class Member {
    @Id
    @GeneratedValue(generator = "jpa-uuid")
    private String id;
    @NotNull
    @Column(name="user_name")
    private String userName;
    @NotNull
    private String password;
    private String name;
    private String email;

}
