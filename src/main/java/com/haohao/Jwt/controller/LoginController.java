package com.haohao.Jwt.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.haohao.Jwt.config.JwtTokenUtil;
import com.haohao.Jwt.entity.Member;
import com.haohao.Jwt.model.MemberModel;
import com.haohao.Jwt.service.MemberService;

import io.jsonwebtoken.ExpiredJwtException;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MemberService memberService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public MemberModel login(@RequestBody Member member)
            throws Exception {

        MemberModel memberModel = null;
        try {
            memberModel = memberService.getMemberByUserName(member.getUserName());

            if (memberModel == null) {
                memberModel = MemberModel.builder().login(false).opmsg("userName is wrong.").build();
            } else if (!memberModel.getPassword().equals(member.getPassword())) {
                memberModel = MemberModel.builder().login(false).opmsg("password is wrong.").build();
            } else {
                memberModel = MemberModel.builder().login(true).opmsg("login success.").build();
            }
        } catch (Exception e) {
            memberModel = MemberModel.builder().login(false).opmsg(e.getMessage()).build();
        }

        if (!memberModel.isLogin()) {
            return memberModel;
        }

        try {
            final String token = jwtTokenUtil.generateToken(memberModel);
            MemberModel.builder().generateJwt(true).opmsg("generate Jwt success").jwt(token);
        } catch (Exception e) {
            MemberModel.builder().generateJwt(false).opmsg("generate Jwt fail");
            e.printStackTrace();
        }
        return memberModel;
    }

    @RequestMapping(value = "/jwtValid", method = RequestMethod.POST)
    public @ResponseBody MemberModel jwtValid(HttpServletRequest request) throws Exception {

        MemberModel memberModel = null;
        
        final String requestTokenHeader = request.getHeader("Authorization");
        System.out.println("requestTokenHeader : " + requestTokenHeader);


        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            String jwt = requestTokenHeader.substring(7);
            try {
                jwtTokenUtil.validateToken(jwt);
                memberModel = MemberModel.builder().jwtValid(true).opmsg("JWT Token is valid").build();
            } catch (IllegalArgumentException e) {
                memberModel = MemberModel.builder().jwtValid(false).opmsg("Unable to get JWT Token").build();
            } catch (ExpiredJwtException e) {
                memberModel = MemberModel.builder().jwtValid(false).opmsg("JWT Token has expired").build();
            } catch (Exception e) {
                memberModel = MemberModel.builder().jwtValid(false).opmsg("JWT Token is invalid").build();
            }
        } else {
            memberModel = MemberModel.builder().jwtValid(false).opmsg("JWT Token does not begin with Bearer String").build();
        }

        return memberModel;
    }
}
