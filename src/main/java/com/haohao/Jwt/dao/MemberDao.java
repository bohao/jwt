package com.haohao.Jwt.dao;

import java.util.List;

import com.haohao.Jwt.entity.Member;

public interface MemberDao {
    List<Member> queryAllMembers();
    
    Member queryMamberByUserName(String userName);
    
    boolean insertMember(Member member);
}
