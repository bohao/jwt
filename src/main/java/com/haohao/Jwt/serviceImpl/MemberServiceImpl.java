package com.haohao.Jwt.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.haohao.Jwt.dao.MemberDao;
import com.haohao.Jwt.entity.Member;
import com.haohao.Jwt.model.MemberModel;
import com.haohao.Jwt.service.MemberService;

@Repository
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberDao memberDao;
    
    @Override
    public List<Member> getAllMembers() {
        return memberDao.queryAllMembers();
    }

    @Override
    public MemberModel getMemberByUserName(String userName) {
    	Member member = memberDao.queryMamberByUserName(userName);
    	return MemberModel.builder().id(member.getId()).userName(member.getUserName()).password(member.getPassword()).email(member.getEmail()).build();
    }

    @Transactional
	@Override
	public boolean addMember(MemberModel memberModel) {
    	Member member = new Member();
    	member.setId(memberModel.getId());
    	member.setUserName(memberModel.getUserName());
    	member.setPassword(memberModel.getPassword());
    	member.setEmail(memberModel.getEmail());
		return memberDao.insertMember(member);
	}
}
